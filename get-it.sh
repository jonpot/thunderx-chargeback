#!/bin/bash

RESOURCE_MANAGER="cavium-rm01.arc-ts.umich.edu:8088"
CURRENT_TIME=$(date +%s%3N) # Epoch time in ms (the units yarn uses)
#PERIOD_DURATION=86400000    # 24 hr period in ms
PERIOD_DURATION=2592000000    # 30 day period in ms

PERIOD_END=${CURRENT_TIME}
PERIOD_START=$((PERIOD_END-PERIOD_DURATION))

echo "Jobs that completed in the last 24 hours:"
echo "---"

# Print table to terminal
#curl -s "http://${RESOURCE_MANAGER}/ws/v1/cluster/apps?finishedTimeBegin=${PERIOD_START}" \
#  | jq -r '.apps.app[] | "\(.id) \(.user) \(.memorySeconds) \(.vcoreSeconds)"' | column -t

# Write to CSV file (redirect stdout to file)
curl -s "http://${RESOURCE_MANAGER}/ws/v1/cluster/apps?finishedTimeBegin=${PERIOD_START}" \
  | jq -r '.apps.app[] | [.id, .user, .memorySeconds, .vcoreSeconds] | @csv'
