############################################################
#
# Built from Yarn's Cluster Applications API
# https://hadoop.apache.org/docs/r2.7.3/hadoop-yarn/hadoop-yarn-site/ResourceManagerRest.html#Application_API

import os, sys, time
from urllib.parse import urlencode
import requests
import sqlite3
import psycopg2
from decimal import Decimal
from configparser import ConfigParser

resourceManagerUrl = "http://cavium-rm01.arc-ts.umich.edu:8088/ws/v1/cluster/apps?"
currentTime = int(time.time()*1000.0)  # Epoch time in ms (the units yarn uses)
durationDayMs = 1000 * 60 * 60 * 24    # Milliseconds in a day
periodDurationDays = 150
periodDurationMs = periodDurationDays * durationDayMs
periodEnd = currentTime
periodStart = periodEnd - periodDurationMs
recordLimit = 6000
pricePerMemorySecond = 0.00000000005
pricePerVcoreSecond = 0.0000005
table_name = "application_history"
db_system = "postgres"                  # "postgres" or "sqlite"

def connect():

    # Get database connection parameters from env vars.
    params = {}

    if 'POSTGRES_HOST' in os.environ:
        params['host'] = os.environ['POSTGRES_HOST']
    else:
        params['host'] = "localhost"

    if 'POSTGRES_DB' in os.environ:
        params['database'] = os.environ['POSTGRES_DB']
    else:
        params['database'] = "yarn_logs"

    if 'POSTGRES_USER' in os.environ:
        params['user'] = os.environ['POSTGRES_USER']
    else:
        params['user'] = "wingedbeast"

    if 'POSTGRES_PASSWORD' in os.environ:
        params['password'] = os.environ['POSTGRES_PASSWORD']
    else:
        print("Error: No env variable POSTGRES_PASSWORD found. Cannot connect to database.")
        sys.exit()

    conn = None
    try:

        if db_system == 'postgres':
            conn = psycopg2.connect(**params)
            c = conn.cursor()

            c.execute('SELECT version()')
            db_version = c.fetchone()
            print('Connected to the PostgreSQL database. Host: ' + params['host'])

        elif db_system == 'sqlite':
            conn = sqlite3.connect('job_history.db')
            c = conn.cursor()
            print('Connected to the SQLite database.')

        else:
            print("Error: Variable db_system not an allowable value.")

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    return conn, c

def create_table( conn, c, db_system ):

    # Check if table exists.
    if db_system == 'postgres':
        sql_string = "SELECT FROM information_schema.tables WHERE table_name = '{}';".format(table_name)
    elif db_system == 'sqlite':
        sql_string = "SELECT name FROM sqlite_master WHERE type='table' AND name='{}';".format(table_name)
    else:
        print("Error: Variable db_system not an allowable value.")

    c.execute(sql_string)

    # Create table if needed.
    if len(c.fetchall()) == 0:
        if db_system == 'postgres':
            columns = {
                'clusterId': 'BIGINT',
                'finalStatus': 'TEXT',
                'unmanagedApplication': 'INT',
                'queueUsagePercentage': 'INT',
                'numAMContainerPreempted': 'INT',
                'vcoreSeconds': 'BIGINT',
                'clusterUsagePercentage': 'INT',
                'trackingUI': 'TEXT',
                'runningContainers': 'INT',
                'preemptedResourceMB': 'INT',
                'id': 'TEXT',
                'applicationTags': 'TEXT',
                'amHostHttpAddress': 'TEXT',
                'elapsedTime': 'BIGINT',
                'amContainerLogs': 'TEXT',
                'logAggregationStatus': 'TEXT',
                'diagnostics': 'TEXT',
                'amNodeLabelExpression': 'TEXT',
                'allocatedMB': 'INT',
                'queue': 'TEXT',
                'allocatedVCores': 'INT',
                'startedTime': 'BIGINT',
                'finishedTime': 'BIGINT',
                'preemptedResourceVCores': 'INT',
                'preemptedMemorySeconds': 'BIGINT',
                'state': 'TEXT',
                'trackingUrl': 'TEXT',
                'preemptedVcoreSeconds': 'INT',
                'progress': 'REAL',
                'amRPCAddress': 'TEXT',
                'memorySeconds': 'BIGINT',
                'uniqname': 'TEXT',
                'name': 'TEXT',
                'applicationType': 'TEXT',
                'priority': 'INT',
                'numNonAMContainerPreempted': 'INT',
                'PRIMARY KEY': '(id, startedTime)'
            }
        elif db_system == 'sqlite':
            columns = {
                'clusterId': 'INTEGER',
                'finalStatus': 'TEXT',
                'unmanagedApplication': 'INTEGER',
                'queueUsagePercentage': 'INTEGER',
                'numAMContainerPreempted': 'INTEGER',
                'vcoreSeconds': 'INTEGER',
                'clusterUsagePercentage': 'INTEGER',
                'trackingUI': 'TEXT',
                'runningContainers': 'INTEGER',
                'preemptedResourceMB': 'INTEGER',
                'id': 'TEXT',
                'applicationTags': 'TEXT',
                'amHostHttpAddress': 'TEXT',
                'elapsedTime': 'INTEGER',
                'amContainerLogs': 'TEXT',
                'logAggregationStatus': 'TEXT',
                'diagnostics': 'TEXT',
                'amNodeLabelExpression': 'TEXT',
                'allocatedMB': 'INTEGER',
                'queue': 'TEXT',
                'allocatedVCores': 'INTEGER',
                'startedTime': 'INTEGER',
                'finishedTime': 'INTEGER',
                'preemptedResourceVCores': 'INTEGER',
                'preemptedMemorySeconds': 'INTEGER',
                'state': 'TEXT',
                'trackingUrl': 'TEXT',
                'preemptedVcoreSeconds': 'INTEGER',
                'progress': 'REAL',
                'amRPCAddress': 'TEXT',
                'memorySeconds': 'INTEGER',
                'uniqname': 'TEXT',
                'name': 'TEXT',
                'applicationType': 'TEXT',
                'priority': 'INTEGER',
                'numNonAMContainerPreempted': 'INTEGER',
                'PRIMARY KEY': '(id, startedTime)'
            }
        else:
            print("Error: Variable db_system not an allowable value.")

        columnAndType = set()
        for key, val in columns.items():
            columnAndType.add(key + " " + val)

        sql_string = 'CREATE TABLE {} '.format(table_name)
        sql_string += "(" + ', '.join(columnAndType) + ");"
        c.execute(sql_string)
        conn.commit()
        print("Created table '{}'.".format(table_name))

    return conn, c

def load_db( conn, c, data, db_system ):

    # We cannot use executemany() because the records returned by
    # the API do not have the same fields. Yarn jobs that fail are
    # missing fields in the API response. So we have to build a
    # unique sql insert statement for each record separately.

    # Trim data into a list where each item in the list is a dictionary
    # representing one record.
    record_list = data["apps"]["app"]

    # Count number of new records inserted.
    count = 0

    # Enumerate over the list of records
    # Each record is a dictionary of key-value pairs
    for i, record in enumerate(record_list):

        # Build the SQL insert command.
        if db_system == 'postgres':
            sql_string = 'INSERT INTO {}'.format( table_name )
        elif db_system == 'sqlite':
            # older versions of SQLite do not support UPSERT
            sql_string = 'INSERT OR IGNORE INTO {}'.format( table_name )
        else:
            print("Error: Variable db_system not an allowable value.")

        # Use each record's keys as column names converting 'user' to 
        # 'uniqname' because 'user' is postgres reserved word.
        keys = list(record.keys())
        columns = ['uniqname' if x=='user' else x for x in keys]

        # enclose the column names within parenthesis
        sql_string += "(" + ', '.join(columns) + ")\nVALUES "

        # Convert one particular key-value from type boolean to integer for SQLite.
        record["unmanagedApplication"] = int(record["unmanagedApplication"])

        # Build a list of values of each record
        values = list(record.values())

        # Build the value substitution string to match the number of values.
        substitution_string = ""
        for i in values:
            substitution_string = substitution_string + '%s,'
        substitution_string = "(" + substitution_string[:-1] + ")"
        sql_string = sql_string + substitution_string

        if db_system == 'postgres':
            c.execute(sql_string + ' ON CONFLICT (id, startedtime) DO NOTHING;', values)
            count = count + c.rowcount
        elif db_system == 'sqlite':
            c.execute(sql_string + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON CONFLICT (id) DO NOTHING;', values)
            count = count + c.rowcount
        else:
            print("Error: Variable db_system not an allowable value.")

    conn.commit()
    print("Loaded {} new records in database.".format(count))

    return

def print_all_records( conn, c ):

    sql_string = "SELECT uniqname,queue,memorySeconds FROM application_history ORDER BY memorySeconds DESC;"
    c.execute(sql_string)

    for row in c.fetchall():
        print(row)

    return

def print_usage_by_user( conn, c ):

    sql_string = "SELECT uniqname,SUM(memorySeconds) FROM application_history GROUP BY uniqname ORDER BY SUM(memorySeconds) DESC;"
    c.execute(sql_string)

    for row in c.fetchall():
        print(row)

    return

def print_usage_by_queue( conn, c ):

    sql_string = "SELECT queue,SUM(memorySeconds),SUM(vcoreSeconds) FROM application_history GROUP BY queue ORDER BY SUM(memorySeconds) DESC;"

    print("\n############################################################################################################")
    print("USAGE BY QUEUE\n")
    print("{:<17} {:^20}     {:^11} {:>20}      {:^11}     {:^11}".format("Queue", "MB-Seconds", "Memory Cost", "Core-Seconds", "Core Cost", "Total Cost"))

    c.execute(sql_string)

    for row in c.fetchall():
        memoryCost = row[1]*Decimal(pricePerMemorySecond)
        vcoreCost = row[2]*Decimal(pricePerVcoreSecond)
        totalCost = memoryCost + vcoreCost
        print("{:<17s} {:>20,}    ${:>11,.2f} {:>20,}    ${:>11,.2f}    ${:>11,.2f}".format(row[0], row[1], memoryCost, row[2], vcoreCost, totalCost))

    return

def print_usage_for_queue( conn, c, queueName ):

    sql_string = "SELECT uniqname,SUM(memorySeconds),SUM(vcoreSeconds) FROM application_history WHERE queue = '{}' GROUP BY uniqname ORDER BY SUM(memorySeconds) DESC;"

    print("\n############################################################################################################")
    print("USAGE FOR QUEUE {} BY USER\n".format(queueName))
    print("{:<17} {:^20}     {:^11} {:>20}      {:^11}     {:^11}".format("User", "MB-Seconds", "Memory Cost", "Core-Seconds", "Core Cost", "Total Cost"))
    c.execute(sql_string.format(queueName))

    for row in c.fetchall():
        memoryCost = row[1]*Decimal(pricePerMemorySecond)
        vcoreCost = row[2]*Decimal(pricePerVcoreSecond)
        totalCost = memoryCost + vcoreCost
        print("{:<17s} {:>20,}    ${:>11,.2f} {:>20,}    ${:>11,.2f}    ${:>11,.2f}".format(row[0], row[1], memoryCost, row[2], vcoreCost, totalCost))

    return

def get_most_used_queues( conn, c, count ):

    most_used_queues = []
    sql_string = "SELECT queue FROM application_history GROUP BY queue ORDER BY SUM(memorySeconds) DESC;"

    c.execute(sql_string)

    for row in c.fetchall():
        most_used_queues.append(row[0])

    return most_used_queues[:count]

def close_db( conn, c ):
    conn.close()

############################################################
# Fetch data from yarn API.
def fetch_data():
    session = requests.Session()
    
    response = session.get(resourceManagerUrl + urlencode({
        'limit': recordLimit,
        'finishedTimeBegin': periodStart
    }))
    
    response.raise_for_status()
    data = response.json()
    print("Retrieved " + str(len(data["apps"]["app"])) + " records from API.")

    return data

def print_data(data):    
       
    for i in data["apps"]["app"]: 
        print("Application ID: " + i["id"])
        print("    User: " + i["uniqname"])
        print("    Queue: " + i["queue"])
        print("    Started: " + str(i["startedTime"]))
        print("    Finished: " + str(i["finishedTime"]))
        print("    Elapsed Time: " + str(i["elapsedTime"]))
        print("    Memory Seconds: " + str(i["memorySeconds"]))
        print("    Core Seconds: " + str(i["vcoreSeconds"]))

##############################

data = fetch_data()
conn, c = connect()
create_table(conn, c, db_system)
load_db(conn, c, data, db_system)
time.sleep(3)
print_usage_by_queue(conn, c)
mostUsedQueues = get_most_used_queues(conn, c, 7)
for i in mostUsedQueues:
    print_usage_for_queue(conn, c, i)

close_db(conn, c)

exit()
