# Chargeback for Cavium ThunderX

An application that gathers cluster usage information and saves it in a database. In the future, the DSI team may use this data for customer showback or customer chargeback. The application is written in Python. It regularly fetches data from the Resource Manager's Yarn API and puts it into a PostgreSQL database. The Yarn API resets when the cluster is restarted, so the data only goes back to 2020 winter maintenance.


## Architecture

The production instance runs in Yottabyte kubernetes, `https://kube.ods.blue.ybrc.arc-ts.umich.edu`, in the `dsi` namespace. It consists of 2 containers, a python container with the application code and a postgres container with the database. There is a k8s service in front of the database container to provide routing to the database for the python application. The postgres container writes to a persistent volume in Yottabyte.

## App Stand-alone Usage

```
python3 ./charge.py

Fetching data from API.
Retrieved 4129 records.
Creating database.
Loading database.
Loaded 3660 records.

############################################################################################################
USAGE BY QUEUE

Queue                  MB-Seconds          Memory Cost         Core-Seconds       Core Cost      Total Cost
default             21,631,522,415,882    $   1,081.58        1,500,830,439    $     750.42    $   1,831.99
umsi-drom           11,904,094,194,187    $     595.20        1,078,075,092    $     539.04    $   1,134.24
twitter-research     7,965,118,684,206    $     398.26          529,847,599    $     264.92    $     663.18
dhillon-group        5,048,507,080,641    $     252.43          256,616,721    $     128.31    $     380.73
umsi-qmei            4,797,352,693,091    $     239.87          248,649,481    $     124.32    $     364.19
umsi-cbudak          1,920,881,663,813    $      96.04          375,155,555    $     187.58    $     283.62
umsi699-cbudak         824,936,959,339    $      41.25           58,225,698    $      29.11    $      70.36
covid                  289,778,608,530    $      14.49           54,825,423    $      27.41    $      41.90
alextsoi-optum         283,980,375,598    $      14.20           26,648,257    $      13.32    $      27.52
umsi699-tepl            75,084,436,113    $       3.75           12,276,856    $       6.14    $       9.89
workshop                67,471,395,842    $       3.37            6,677,365    $       3.34    $       6.71
lsa-ebruch              57,353,374,040    $       2.87            5,611,416    $       2.81    $       5.67
arcts                   28,112,476,623    $       1.41            5,472,839    $       2.74    $       4.14
umsi330                 11,289,373,954    $       0.56            2,204,645    $       1.10    $       1.67
eecs476                  1,285,647,059    $       0.06              160,520    $       0.08    $       0.14
cscar                       47,138,536    $       0.00                9,202    $       0.00    $       0.01

############################################################################################################
USAGE FOR QUEUE default BY USER

User                   MB-Seconds          Memory Cost         Core-Seconds       Core Cost      Total Cost
patpark              7,219,468,216,810    $     360.97          297,907,372    $     148.95    $     509.93
ingomez              3,931,979,677,478    $     196.60          260,726,284    $     130.36    $     326.96
maolee               3,358,610,349,622    $     167.93          241,089,752    $     120.54    $     288.48
xresnick             2,147,842,916,069    $     107.39          210,175,487    $     105.09    $     212.48
jianxuny             1,394,713,506,920    $      69.74          136,540,633    $      68.27    $     138.01
caoa                 1,220,177,888,919    $      61.01          119,573,950    $      59.79    $     120.80
xiangxi                548,719,068,098    $      27.44           53,688,094    $      26.84    $      54.28
```

## Environment Configuration

The python application and the database both their configuration from environment variables. In production, these variables are set in k8s `yaml` files and through a k8s secret holding the database password. The values in the `yaml` files do not need to be modified, but when you set up the database, you will create a k8s secret with a strong password. The next section has instruction for that.

For development, the database should be run locally in a docker container and the variables are set as options on the `docker run` command. Running the python app locally will require setting the env variables in your local shell ahead of time. The variables for local development are included in the `.env` file and can be exported to the local environment with these instructions.

```bash
export $(xargs < .env)
```

## Database Overview

In production, the application writes to a Postgres database hosted in k8s. These instructions show how to initially deploy the database container and create the empty database.

```bash
# Create the PVC where data will be persisted.
kubectl create -f ./postgres-pvc.yaml

# Create database password as k8s secret. Note that -n is important to prevent
# appending newline character to end of password.
echo -n changeME493 > password.txt
kubectl create secret generic postgres-pass --from-file=password.txt

# Deploy database pod and service.
kubectl create -f ./postgres-app.yaml
kubectl create -f ./postgres-service.yaml
```

Once the database container is running, these instructions show how to use `psql` to confirm the database instance exists. The python application will fail if the `yarn_logs` database instance is not available.

```bash
# Check the yarn_logs database on the postgres container.
POD=`kubectl get pods | grep -i postgres | grep -i running | awk '{print $1}'`
kubectl exec -it ${POD} -- /bin/bash
psql -U ${POSTGRES_USER} ${POSTGRES_DB}
\l
\q
exit
```

For local development, run the postgres container locally with docker.

```bash
export $(xargs < .env)

docker run --name postgres-dev --env POSTGRES_USER=${POSTGRES_USER} --env POSTGRES_PASSWORD=${POSTGRES_PASSWORD} \
    --env POSTGRES_DB=${POSTGRES_DB} \
    --env PGDATA=/var/lib/postgresql/data/pgdata \
    -d -p 127.0.0.1:5432:5432/tcp postgres:9.5

# Create the yarn_logs database in the running container.
docker exec -it postgres-dev createdb -U ${POSTGRES_USER} -T template0 ${POSTGRES_DB}

# Stop the DB container when done
docker stop postgres-dev
```

## Backing Up and Restoring Database

Postgres writes data to a persistent volume mounted at `/var/lib/postgresql/data`. If the container crashes, the data will still be available on the persistent volume when a new container starts. However, we still need backups of the data should the data ever become corrupt or the persistent volume fail. Currently, we use SQL dump to regularly create *"a text file with SQL commands that, when fed back to the server, will recreate the database in the same state as it was at the time of the dump."* See PostgreSQL [documentation](https://www.postgresql.org/docs/9.5/backup-dump.html) for more details.

We have a cronjob that runs on `cavium-rm01` to regularly run `pg_dump` in the postgres container and save the output to `/backups/cavium/usage-backups/`. It does something similar to the manual instructions below.

```bash
export $(xargs < .env)

# Launch a shell on the postgres container.
# Save SQL dump to file.
POD=`kubectl get pods | grep -i postgres | grep -i running | awk '{print $1}'`
BACKUP_FILE="chargeback.backup.$(date +"%Y.%m.%d")"
kubectl exec ${POD} -- pg_dump -U ${POSTGRES_USER} ${POSTGRES_DB} > ${BACKUP_FILE}

# Move the file to backup location.
scp ${BACKUP_FILE} jonpot@cavium-thunderx.arc-ts.umich.edu:/home/jonpot/
```

To restore the database from the SQL dump file, follow these instructions.

```bash
export $(xargs < .env)

# Create the yarn_logs database on the postgres container.
POD=`kubectl get pods | grep -i postgres | grep -i running | awk '{print $1}'`
kubectl exec -it ${POD} -- createdb -U ${POSTGRES_USER} -T template0 ${POSTGRES_DB}

# Restore from backup file.
export $(xargs < .env)

BACKUP_FILE="chargeback.backup.YY.MM.DD"
kubectl exec -it ${POD} -- psql -U ${POSTGRES_USER} ${POSTGRES_DB} < ${BACKUP_FILE}

# Check the data.
kubectl exec -it ${POD} -- /bin/bash
psql
SELECT COUNT(*) FROM application_history;
\q
```

Newer version of k8s will offer volume snapshots for backing up databases; however, this capability is currently unavailable on our k8s instance. Below are the instructions for taking a snapshot and the instructions for restoring the snapshot if this capability is offered on our k8s instance in the future. For now, use the methods above for backup and restore and ignore the below.

```bash
# Creates a copy of the persistent volume.
kubectl create -f ./postgres-snap.yaml

# View snapshot.
kubectl get volumesnapshot,volumesnapshotdatas

# Restore database by creating a new pvc containing the snapshot data.
kubectl create -f px-snap-pvc.yaml

# Create a new database pod that references the new pvc with the snapshot data.
kubectl create -f postgres-app-restore.yaml
```
## Testing App Locally

Once you have the database running locally in a docker container, you can run the python program with these instructions.

```bash
# Set up env vars for database connection
export $(xargs < .env)

python3 charge.py
```

## Building App Container

The python app runs in a k8s container. This container is built from the `Dockerfile`. Use the helper script to build the image and push it to Dockerhub.com.

```bash
./build-container.sh
```

## Deploying App Container

The python app is run as a k8s cron job. The cron job object will create a daily job that will run the python app in a new container. The python app will download all records from the yarn API and load any new records in the database. The container then terminates. Use these instructions to create the python app cron job.

```bash
# Create the cronjob object.
kubectl create -f ./python-app-cronjob.yaml

# View the jobs.
kubectl get pods
kubectl logs <POD_NAME>
```

Optionally, the python app can also be deployed in k8s as a long-running container with these instructions. However, this deployment is not needed when using the cron job approach.

```bash
kubectl create -f ./python-app.yaml
```

Manually run an API query and database update.

```bash
POD=`kubectl get pods | grep -i python | grep -i running | awk '{print $1}'`
kubectl exec -it ${POD} -- bash

cd /app/
python3 charge.py
```

## References

- https://hakibenita.com/fast-load-data-python-postgresql
- https://realpython.com/python-json/
- https://realpython.com/prevent-python-sql-injection/
- https://kb.objectrocket.com/postgresql/insert-json-data-into-postgresql-using-python-part-2-1248
- https://katacoda.com/portworx/scenarios/px-k8s-postgres-all-in-one
