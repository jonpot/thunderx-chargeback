#!/usr/bin/env bash
#
# Builds container image.

IMAGE_REGISTRY=docker.io
ORG_NAME=jonathanpotter
IMAGE_NAME=chargeback
IMAGE_TAG=1.0.0

build_container() {
    # Build the container image
    docker build -f Dockerfile -t ${IMAGE_NAME}:${IMAGE_TAG} .

    # Push image to registry
    docker login https://${IMAGE_REGISTRY}
    docker tag ${IMAGE_NAME}:${IMAGE_TAG} ${IMAGE_REGISTRY}/${ORG_NAME}/${IMAGE_NAME}:${IMAGE_TAG}
    docker push ${IMAGE_REGISTRY}/${ORG_NAME}/${IMAGE_NAME}:${IMAGE_TAG}
}

############################
# Main
############################

build_container
