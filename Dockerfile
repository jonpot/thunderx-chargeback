# Get python base image.
FROM docker.io/python:3.7.4

# Install python packages as needed.
ARG PACKAGES="requests psycopg2"
RUN python3 -m pip install ${PACKAGES}

# Copy the app to the image.
RUN mkdir /app
COPY charge.py /app/
